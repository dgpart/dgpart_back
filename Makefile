.PHONT: up down restart build init

up:
	docker-compose -f docker/docker-compose.yml up -d

down:
	docker-compose -f docker/docker-compose.yml down

restart: down up

build:
	docker-compose -f docker/docker-compose.yml build

init:
	include ./docker/.env
	export $(shell sed 's/=.*//' .env)
	docker network create ${NETWORK}
