FROM nginx/unit:1.23.0-php8.0 as base

RUN mkdir /usr/lib/unit/state/
RUN mkdir /usr/lib/unit/state/certs/

ENTRYPOINT ["/usr/local/bin/docker-unit.sh"]
CMD ["unitd", "--no-daemon", "--control", "unix:/var/run/control.unit.sock", "--pid", "/var/run/unit.pid", "--log", "/var/log/unit.log", "--modules", "/usr/lib/unit/modules", "--state", "/usr/lib/unit/state/", "--tmp", "/var/tmp/"]
COPY docker-unit.sh /usr/local/bin/
COPY docker-entrypoint.sh docker-unit-config.json /docker-entrypoint.d/

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar composer && chmod a+x composer && mv composer /usr/local/bin/

#ENV PHP_VERSION         8.0
#
#RUN apt-get update \
#    && apt install apt-transport-https lsb-release ca-certificates wget curl gnupg gnupg2 gnupg1 -y \
#    && wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
#    && sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' \
#    && apt update

RUN apt update && apt install -y -q --no-install-recommends \
                    libpq-dev \
                    git \
                    vim \
                    nano \
                    less \
                    mc \
                    wget \
                    zip \
                    unzip \
                    apt-utils \
                    procps

RUN docker-php-ext-install pdo_mysql pdo_pgsql

COPY install_php_extensions.sh /tmp/scripts/
RUN chmod +x -R /tmp/scripts/
RUN /tmp/scripts/install_php_extensions.sh

RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony


FROM base as xdebug
RUN apt update \
    && apt install php${PHP_VERSION}-xdebug \
    && echo "xdebug.remote_enable = 1"                 >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \

FROM base as console
CMD php ./console/yii ${CONSOLE} *${HOSTNAME}

FROM base as xdebug
RUN apt update \
    && apt install php${PHP_VERSION}-xdebug \
    && echo "xdebug.remote_enable = 1"                 >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.remote_autostart = on"             >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.remote_connect_back = 1"           >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.remote_port = 9000"                >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.var_display_max_depth = -1"        >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.var_display_max_children = -1"     >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.var_display_max_data = -1"         >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.max_nesting_level = 500"           >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini \
    && echo "xdebug.remote_host = 127.0.0.1"           >> /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini

