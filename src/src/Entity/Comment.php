<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $postID;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postUuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userUuid;

    /**
     * @ORM\Column(type="integer")
     */
    private $parentID;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="date")
     */
    private $dt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostID(): ?int
    {
        return $this->postID;
    }

    public function setPostID(int $postID): self
    {
        $this->postID = $postID;

        return $this;
    }

    public function getPostUuid(): ?string
    {
        return $this->postUuid;
    }

    public function setPostUuid(string $postUuid): self
    {
        $this->postUuid = $postUuid;

        return $this;
    }

    public function getUserUuid(): ?string
    {
        return $this->userUuid;
    }

    public function setUserUuid(string $userUuid): self
    {
        $this->userUuid = $userUuid;

        return $this;
    }

    public function getParentID(): ?int
    {
        return $this->parentID;
    }

    public function setParentID(int $parentID): self
    {
        $this->parentID = $parentID;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDt(): ?\DateTimeInterface
    {
        return $this->content;
    }

    public function setDt(?\DateTimeInterface $content): self
    {
        $this->content = $content;

        return $this;
    }
}
