<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Post;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController {

    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    public function add(Request $request) : JsonResponse
    {
        $post = new Post();
        $post->setUuid(Uuid::v1()->toRfc4122());
        $post->setCategoryID($request->get('categoryID'));
        $post->setTitle($request->get('title'));
        $post->setDt(new \DateTime());
        $post->setIsDeleted(false);
        $this->em->persist($post);
        $this->em->flush();

        return new JsonResponse(
            $this->serializer->serialize($post, JsonEncoder::FORMAT),
            Response::HTTP_CREATED,
            [],
            true
        );
    }

    public function delete(Request $request) : JsonResponse
    {
        $uuid = $request->get('uuid');
        $post = $this->em->getRepository(Post::class)
            ->findOneBy(['uuid' => $uuid]);

        if (!$post) {
            return new JsonResponse(
                ['error' => 'No post found for uuid '.$uuid],
                Response::HTTP_NOT_FOUND,
            );
        }

        if ($post->getIsDeleted() === true) {
            return new JsonResponse(
                ['error' => 'No post is delete for uuid '.$uuid],
                Response::HTTP_NO_CONTENT
            );
        }

        $post->setIsDeleted(true);
        $this->em->persist($post);
        $this->em->flush();

        return new JsonResponse(
            $this->serializer->serialize($post, JsonEncoder::FORMAT),
            Response::HTTP_ACCEPTED,
            [],
            true
        );
    }

}

